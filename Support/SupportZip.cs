﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.CompilerServices;
using Support.Analysis;
using Support.Analysis.EventLogs;
using Support.Analysis.Settings;
using Support.Error;

[assembly: InternalsVisibleTo("SupportTest")]
namespace Support
{
    public class SupportZip
    {
        private static readonly string[] _expectedFiles = new[]
            {"message.txt", "appSettings.config", "iTernity.EventLog"};

        public List<EventLogHtml> EventLogHtmls = new List<EventLogHtml>();

        public MessageFile MessageFile { get; private set; }
        public Changes ConfigChanges { get; private set; }
        public List<AppSettings> AppSettings { get; private set; }
        
        private ZipArchive openZipFile(string filePath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                var zip = new ZipArchive(fs, ZipArchiveMode.Read, false);
                foreach (var expectedFile in _expectedFiles)
                {
                    if (!zip.Entries.Any(entry => entry.Name.Contains(expectedFile)))
                    {
                        zip.Dispose();
                        throw new InvalidZipException("Zip is not an iCAS Support zip.");
                    }
                }
                return zip;
            }
            catch
            {
                fs?.Dispose();
                throw;
            }
        }

        public void Open(string zipFile)
        {
            try
            {
                EventLogHtmls = new List<EventLogHtml>();
                MessageFile = null;
                ConfigChanges = null;
                using (var zip = openZipFile(zipFile))
                {
                    analyze(zip);
                }
            }
            catch (InvalidZipException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new InvalidZipException("Unable to open zip", ex);
            }
        }

        protected virtual void analyze(ZipArchive zip)
        {
            parseMessages(zip);
            parseChanges(zip);
            parseEventLogs(zip);
            parseAppSettings(zip);
        }

        protected virtual void parseAppSettings(ZipArchive zip)
        {
            AppSettings = zip.Entries
                .Where(entry => entry.Name.Contains("appSettings"))
                .Select(entry =>
                {
                    using (var s = entry.Open())
                    {
                        return new AppSettings(s, entry.Name);
                    }
                })
                .ToList();
        }

        protected virtual void parseChanges(ZipArchive zip)
        {
            ConfigChanges = new Changes(zip.Entries.First(
                e => e.Name.Contains("User.config.Changes")).Open());
        }

        protected virtual void parseMessages(ZipArchive zip)
        {
            MessageFile = new MessageFile(zip.Entries
                .First(e => e.Name.Contains("message.txt"))
                .Open());
        }

        protected virtual void parseEventLogs(ZipArchive zip)
        {
            var evtLogHtmls = zip.Entries
                .Where(e => e.Name.Contains("iTernity.Event"));
            foreach (var evtLogHtml in evtLogHtmls)
            {
                using (var stream = evtLogHtml.Open())
                {
                    EventLogHtmls.Add(new EventLogHtml(stream, evtLogHtml.Name));
                }
            }
        }
    }
}
