﻿using System;

namespace Support.Events
{
    public class ProgressMadeEventArgs : EventArgs
    {
        private readonly int _currentIdx;
        private readonly int _allIdx;

        public int ProgressPercent => calculatePercentage();
        public string ProgressMessage { get; }
        
        public ProgressMadeEventArgs(string message, int currentIdx, int allIdx)
        {
            ProgressMessage = message;
            _currentIdx = currentIdx;
            _allIdx = allIdx;
        }

        private int calculatePercentage()
        {
            var percentFloat = (float) 100 * _currentIdx / _allIdx;
            return (int) percentFloat;
        }
    }
}