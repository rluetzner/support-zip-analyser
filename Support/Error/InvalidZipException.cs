﻿using System;

namespace Support.Error
{
    internal class InvalidZipException : Exception
    {
        public InvalidZipException(string msg) : base(msg) { }
        public InvalidZipException(string msg, Exception ex) : base(msg, ex) { }
    }
}