﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Support.Analysis.Settings
{
    public class AppSettings
    {
        #region Web Service Defaults
        private static readonly Dictionary<string, string> web_service_app_settings = new Dictionary<string, string>
        {
            {"InstallPath", @"C:\Program Files\iTernity"},
            {"DemoLicenseRepository", "s3"},
            {"LoggingPath", @"C:\iTernity\logs"},
            {"ConfigPath", @"C:\Program Files\iTernity\iServer"},
            {"ObjectStorageConfigPath", @"C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"}
        };
        #endregion

        #region iTernityService Defaults
        private static readonly Dictionary<string, string> iternity_service_app_settings = new Dictionary<string, string>
        {
            {"Logging", "0"},
            {"TempPath", @"C:\Windows\Temp"},
            {"LoggingPath", @"C:\iTernity\logs"},
            {"ConfigPath", @"C:\Program Files\iTernity\iServer"},
            {"ObjectStorageConfigPath", @"C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"}
        };
        #endregion

        #region iFSG Defaults
        private static readonly Dictionary<string, string> ifsg_app_settings = new Dictionary<string, string>
        {
            {"Logging", "0"},
            {"LoggingType","Rolling" },
            {"MetaDataCache","default" },
            {"MinFileLengthForThreading","default" },
            {"MinThreshold","default" },
            {"MaxThreshold","default" },
            {"MountNetworkStorage","default" },
            {"StorageCharacteristics","default" },
            {"SerializeCallbacks","default" },
            {"SleepWait","default" },
            {"StubPoolSize","default" },
            {"ThreadPoolSize","default" },
            {"UseSystemCache","default" },
            {"EnableForcedRenaming","default" },
            {"CachePath", @"C:\Windows\Temp\iCache"},
            {"LoggingPath", @"C:\iTernity\logs"},
            {"ConfigPath", @"C:\Program Files\iTernity\iServer"},
            {"ObjectStorageConfigPath", @"C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"}
        };
        #endregion

        private List<KeyValuePair<string, string>> _appSettings;
        private readonly string _source;

        public AppSettings(Stream content, string name)
        {
            parseAppSettings(content);
            switch (name)
            {
                case "appSettings.config":
                    _source = "Web Service";
                    compareWith(web_service_app_settings);
                    break;
                case "appSettings.iFSG.config":
                    _source = "iFSG";
                    compareWith(ifsg_app_settings);
                    break;
                case "appSettings.iTernityService.config":
                    _source = "iTernityService";
                    compareWith(iternity_service_app_settings);
                    break;
            }
        }

        private void parseAppSettings(Stream content)
        {
            var xEle = XElement.Load(content);
            _appSettings = xEle.Elements()
                .Select(node => node.Attributes())
                .Select(attr =>
                {
                    var key = attr.First(a => a.Name == "key").Value;
                    var value = attr.First(a => a.Name == "value").Value;
                    return new KeyValuePair<string, string>(key, value);
                }).ToList();
        }

        private IEnumerable<string> keys => _appSettings.Select(sett => sett.Key);

        private void compareWith(Dictionary<string, string> defaultAppSettings)
        {
            findMissingSettings(defaultAppSettings);
            findAdditionalSettings(defaultAppSettings);
            findChangedSettings(defaultAppSettings);
        }

        private void findChangedSettings(Dictionary<string, string> defaultAppSettings)
        {
            var same = keys.Where(key => defaultAppSettings.Keys.Contains(key));
            foreach (var key in same)
            {
                var currentValue = _appSettings.First(kvp => kvp.Key == key);
                var webServiceValue = defaultAppSettings[key];
                if (currentValue.Value != webServiceValue)
                {
                    DifferingSettings.Add(new DifferingSetting(_source, currentValue.Key, currentValue.Value,
                        webServiceValue));
                }
            }
        }

        private void findAdditionalSettings(Dictionary<string, string> defaultAppSettings)
        {
            var added = keys
                            .Where(key => !defaultAppSettings.Keys.Contains(key))
                            .Select(key => _appSettings.First(kvp => kvp.Key == key))
                            .Select(kvp => new DifferingSetting(_source, kvp.Key, kvp.Value, string.Empty));
            DifferingSettings.AddRange(added);
        }

        private void findMissingSettings(Dictionary<string, string> defaultAppSettings)
        {
            var missing = defaultAppSettings.Keys
                                        .Where(key => !keys.Contains(key))
                                        .Select(key => (Key: key, Value: defaultAppSettings[key]))
                                        .Select(kvp => new DifferingSetting(_source, kvp.Key, string.Empty, kvp.Value));
            DifferingSettings.AddRange(missing);
        }

        public List<DifferingSetting> DifferingSettings { get; } = new List<DifferingSetting>();
    }
}
