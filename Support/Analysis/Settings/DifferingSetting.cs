﻿namespace Support.Analysis.Settings
{
    public class DifferingSetting
    {
        public DifferingSetting(string source, string key, string customerValue, string originalValue)
        {
            Source = source;
            Key = key;
            CustomerValue = customerValue;
            OriginalValue = originalValue;
        }

        public string Source { get; }
        public string Key { get; }
        public string CustomerValue { get; }
        public string OriginalValue { get; }
    }
}