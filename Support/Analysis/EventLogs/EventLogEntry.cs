﻿using System;

namespace Support.Analysis.EventLogs
{
    public class EventLogEntry
    {
        public string EntryType { get; set; }
        public int Category { get; set; }
        public int EventId { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
    }
}