﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using HtmlAgilityPack;

namespace Support.Analysis.EventLogs
{
    public class EventLogHtml
    {
        public List<EventLogEntry> EventLogEntries { get; } = new List<EventLogEntry>();
        public string Name { get; }

        public EventLogHtml(Stream htmlContent, string name)
        {
            Name = name;
            using (var streamReader = new StreamReader(htmlContent))
            {
                parseAllEvents(streamReader);
            }
        }

        private void parseAllEvents(TextReader reader)
        {
            var tables = getTableElements(reader);
            if (tables == null || tables.Count == 0) return;
            foreach (var node in tables)
            {
                var (res, entry) = tryParseEventFromTable(node);
                if (res)
                {
                    EventLogEntries.Add(entry);
                }
            }
        }

        private static HtmlNodeCollection getTableElements(TextReader reader)
        {
            var html = new HtmlDocument();
            html.Load(reader);
            var ele = html.DocumentNode.SelectNodes("/html/body/p/table");
            return ele;
        }

        private static (bool CouldParse, EventLogEntry Entry) tryParseEventFromTable(HtmlNode node)
        {
            var nodeCollection = node.SelectNodes("tr/td");
            try
            {
                if (nodeCollection.Count < 12)
                {
                    return (false, null);
                }
                var entry = parseTable(nodeCollection);
                return (true, entry);
            }
            catch
            {
                return (false, null);
            }
        }

        private static EventLogEntry parseTable(HtmlNodeCollection nodeCollection)
        {
            string cleanUpHtml(string html)
            {
                return html.Replace("<strong>", "").Replace("</strong>", "").Replace("<br>", Environment.NewLine);
            }

            var entry = new EventLogEntry();
            for (var i = 0; i + 1 < nodeCollection.Count; i += 2)
            {
                var typeInnerHtml = nodeCollection[i].InnerHtml;
                var valueInnerHtml = cleanUpHtml(nodeCollection[i + 1].InnerHtml);
                if (typeInnerHtml.Contains("EntryType"))
                {
                    entry.EntryType = valueInnerHtml;
                }
                else if (typeInnerHtml.Contains("Category"))
                    try {
                        entry.Category = int.Parse(valueInnerHtml.Replace("(", "").Replace(")", ""));
                    }
                    catch
                    {
                        entry.Category = -1;
                    }
                else if (typeInnerHtml.Contains("InstanceId"))
                    try {
                        entry.EventId = int.Parse(valueInnerHtml);
                    }
                    catch
                    {
                        entry.EventId = -1;
                    }
                else if (typeInnerHtml.Contains("TimeGenerated"))
                {
                    if (DateTime.TryParse(valueInnerHtml, out var date))
                    {
                        entry.Date = date;
                    }
                    else if (DateTime.TryParse(valueInnerHtml, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out date))
                    {
                        entry.Date = date;
                    }
                    else
                    {
                        entry.Date = DateTime.MinValue;
                    }
                }
                else if (typeInnerHtml.Contains("Message")) entry.Message = valueInnerHtml.Replace("\"", "'");
            }

            return entry;
        }
    }
}
