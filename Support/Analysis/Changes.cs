﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Support.Analysis
{
    public class Changes
    {
        private const string SAVE_CONFIG_NAME = "SaveConfig";
        public List<SavedChange> SavedChanges = new List<SavedChange>();

        public Changes(Stream content)
        {
            using (var sr = new StreamReader(content))
            {
                parseChanges(sr);
            }
        }

        private void parseChanges(TextReader reader)
        {
            var tables = getTablesFromHtml(reader);
            foreach (var table in tables)
            {
                var tableRows = table[0].SelectNodes("tr");
                for (int i = 0; i < tableRows.Count; i++)
                {
                    var currentRow = tableRows[i];
                    if (currentRow.InnerHtml.Contains(SAVE_CONFIG_NAME))
                    {
                        var configSave = parseConfigSave(tableRows, i);
                        SavedChanges.Add(configSave);
                    }
                }
            }
        }

        private IEnumerable<HtmlNodeCollection> getTablesFromHtml(TextReader reader)
        {
            var content = reader.ReadToEnd();
            var tableSubStrings = new List<string>();
            var indexFound = true;
            do
            {
                const string tableOpenString = "<table";
                var tableOpen = content.IndexOf(tableOpenString);
                const string tableCloseString = "</table>";
                var tableClose = content.IndexOf(tableCloseString);
                indexFound = tableOpen >= 0;
                if (indexFound)
                {
                    tableSubStrings.Add(content.Substring(tableOpen, tableClose + tableCloseString.Length - tableOpen));
                    content = content.Remove(0, tableClose + tableCloseString.Length);
                }
            } while (indexFound);
            return tableSubStrings.Select(tables => new StringReader(tables))
                .Select(r =>
                {
                    var html = new HtmlDocument();
                    html.Load(r);
                    return html;
                })
                .Select(h => h.DocumentNode.SelectNodes("table"));                
        }

        private SavedChange parseConfigSave(HtmlNodeCollection tableRows, int currentIndex)
        {
            var change = new SavedChange();
            var currentRow = tableRows[currentIndex];
            var subNodes = currentRow.SelectNodes("td");
            if (subNodes.Count > 2)
            {
                var dateEntry = subNodes[2].InnerHtml;
                if (DateTime.TryParse(dateEntry, out var date))
                {
                    change.SaveTime = date;
                }
                else if (DateTime.TryParse(dateEntry, CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None, out date))
                {
                    change.SaveTime = date;
                }
                else
                {
                    change.SaveTime = DateTime.MinValue;
                }
            }
            for (int j = currentIndex + 1; j < tableRows.Count; j++)
            {
                currentRow = tableRows[j];
                if (currentRow.InnerHtml.Contains(SAVE_CONFIG_NAME)) break;
                else
                {
                    subNodes = currentRow.SelectNodes("td");
                    var key = subNodes[0].SelectNodes("ul/li")[0].InnerHtml;
                    var oldValue = subNodes[1].InnerHtml;
                    var newValue = subNodes[2].InnerHtml;
                    change.AddChange(key, oldValue, newValue);
                }
            }
            return change;            
        }
    }

    public class SavedChange
    {
        public DateTime SaveTime { get; set; }
        public List<(string Key, ChangeValue Change)> ChangedValues { get; set; } = new List<(string, ChangeValue)>();

        public void AddChange(string key, string oldValue, string newValue)
        {
            ChangedValues.Add((key, new ChangeValue
            {
                Old = oldValue,
                New = newValue
            }));
        }
    }

    public class ChangeValue
    {
        public string Old { get; set; }
        public string New { get; set; }
    }
}
