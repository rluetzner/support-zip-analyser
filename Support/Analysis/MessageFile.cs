﻿using System;
using System.IO;
using System.Text;

namespace Support.Analysis
{
    public class MessageFile
    {
        public string iCASVersion { get; private set; }
        public string DotNetVersion { get; private set; }
        public string LicenseeName { get; private set; }
        public string LicenseeMail { get; private set; }
        public string LicenseeAddress { get; private set; }
        public string LicenseNumber { get; private set; }
        public string OperatingSystem { get; private set; }
        public string Virtualisation { get; private set; }
        public string Details { get; private set; }
        public bool ClusterFeatureInstalled { get; private set; }
        public bool ServicesAreInCluster { get; private set; }

        public MessageFile(Stream content)
        {
            parseFile(content);
        }

        private void parseFile(Stream content)
        {
            var sr = new StreamReader(content);
            var line = "";
            while ((line = sr.ReadLine()) != null)
            {
                if (line.Contains("Licensee:")) LicenseeName = splitAndGetCleanEntry(line);
                else if (line.Contains("Licensee Mail:")) LicenseeMail = splitAndGetCleanEntry(line);
                else if (line.Contains("Licensee Address:")) LicenseeAddress = splitAndGetCleanEntry(line);
                else if (line.Contains("License Number:")) LicenseNumber = splitAndGetCleanEntry(line);
                else if (line.Contains("OS:")) OperatingSystem = splitAndGetCleanEntry(line);
                else if (line.Contains(".Net Version:")) DotNetVersion = splitAndGetCleanEntry(line);
                else if (line.Contains("iCAS Version:")) iCASVersion = splitAndGetCleanEntry(line);
                else if (line.Contains("Virtualisation:")) Virtualisation = splitAndGetCleanEntry(line);
                else if (line.Contains("Details:"))
                {
                    var sb = new StringBuilder();
                    while ((line = sr.ReadLine())!=null)
                    {
                        if (string.IsNullOrEmpty(line)) break;
                        if (line.Contains("--- Performance ---")) break;
                        if (line.Contains("--- Support Info ---")) break;
                        sb.AppendLine(line.Trim());
                    }
                    Details = sb.ToString().Trim('\r', '\n');
                }
                else if (line.Contains("Cluster Feature installed:")) ClusterFeatureInstalled = parseBool(line);
                else if (line.Contains("iCAS Services in Cluster:")) ServicesAreInCluster = parseBool(line);
            }
        }

        private static string splitAndGetCleanEntry(string line)
        {
            var split = line.Split(':');
            return split.Length > 1 ? split[1].Trim() : "";
        }

        private static bool parseBool(string line)
        {
            var boolStr = splitAndGetCleanEntry(line);
            return bool.TryParse(boolStr, out var res) && res;            
        }
    }
}
