﻿namespace Support.Analysis.Stats
{
    public class MonthStatistic
    {
        public int Month { get; }
        public long ObjectCount { get; }
        public long CscCount { get; }
        public long Bytes { get; }

        public MonthStatistic(int month, long objectCount, long cscCount, long bytes)
        {
            Month = month;
            ObjectCount = objectCount;
            CscCount = cscCount;
            Bytes = bytes;
        }
    }
}