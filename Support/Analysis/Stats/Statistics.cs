﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Support.Analysis.Stats
{
    public class Statistics
    {
        public List<YearStatistic> Years { get; } = new List<YearStatistic>();
        public float AverageFileSize { get; }
        public float FilesPerMonth { get; }

        public Statistics(Stream content)
        {
            var xEle = XElement.Load(content);

            Years.AddRange(xEle.Descendants("Year").Select(year =>
            {
                var y = new YearStatistic(int.Parse(year.Attribute("Name").Value));
                y.Months.AddRange(year.Descendants("Month").Select(x => new MonthStatistic(
                    int.Parse(x.Attribute("Name").Value.Substring(4,2)),
                    long.Parse(x.Attribute("ObjectCount").Value),
                    long.Parse(x.Attribute("CSCCount").Value),
                    long.Parse(x.Value))));
                return y;
            }));
            var nonEmptyMonths = Years.SelectMany(y => y.Months).Where(m => m.Bytes > 0).ToList();
            var cscCount = (float) nonEmptyMonths.Sum(m => m.CscCount);
            AverageFileSize = nonEmptyMonths.Sum(m => m.Bytes) / cscCount;
            FilesPerMonth = cscCount / nonEmptyMonths.Count;
        }
    }
}
