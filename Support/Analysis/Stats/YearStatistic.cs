﻿using System.Collections.Generic;

namespace Support.Analysis.Stats
{
    public class YearStatistic
    {
        public int Year { get; }
        public List<MonthStatistic> Months { get; } = new List<MonthStatistic>();

        public YearStatistic(int year) => Year = year;
    }
}