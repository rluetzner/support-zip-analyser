﻿using NUnit.Framework;
using Support;
using System.IO;
using System.Linq;

namespace SupportTest.Test
{
    [TestFixture]
    public class SupportZipAnalysisTests
    {
        private static string _zipPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Files", "iCAS-Support-20181024093728.zip");

        [Test]
        public void AnalyzeRealSupportZip()
        {
            var zip = new SupportZip();
            zip.Open(_zipPath);

            Assert.AreEqual(3, zip.EventLogHtmls.Count);
            var expectedNames = new[] {"Application", "System", "iTernity"};
            foreach (var detectedHtmls in zip.EventLogHtmls)
            {
                Assert.IsTrue(expectedNames
                    .Any(expectedName => detectedHtmls.Name.Contains(expectedName)));
            }

            Assert.IsNotNull(zip.MessageFile);
            Assert.IsNotNull(zip.ConfigChanges);
        }
    }
}
