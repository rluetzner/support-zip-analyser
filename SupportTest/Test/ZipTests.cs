﻿using System.IO;
using System.IO.Compression;
using System.Text;
using NUnit.Framework;
using Support;
using Support.Error;

namespace SupportTest.Test
{
    [TestFixture]
    public class ZipTests
    {
        private string _testPath = Path.Combine(TestContext.CurrentContext.TestDirectory, nameof(ZipTests));

        [SetUp]
        public void SetUp()
        {
            Directory.CreateDirectory(_testPath);
        }

        [TearDown]
        public void TearDown()
        {
            Directory.Delete(_testPath, true);
        }

        [Test]
        public void OpenInvalidZip()
        {
            var invalidZipFile = Path.Combine(_testPath, "invalid.zip");
            File.WriteAllText(invalidZipFile, "This is not a valid zip file.");
            Assert.Throws<InvalidZipException>(() =>
            {
                var zip = new SupportZip();
                zip.Open(invalidZipFile);
            });
        }

        [Test]
        public void OpenValidButNotSupportZip()
        {
            var validZipFile = Path.Combine(_testPath, "valid.zip");
            using (var fs = new FileStream(validZipFile, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                using (var validZip = new ZipArchive(fs, ZipArchiveMode.Create, true))
                {
                    var memStream = new MemoryStream(Encoding.UTF8.GetBytes("content"));
                    var entry = validZip.CreateEntry("content.txt", CompressionLevel.NoCompression);
                    using (var writeStream = entry.Open())
                    {
                        var bytes = Encoding.UTF8.GetBytes("content");
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }
            }

            Assert.Throws<InvalidZipException>(() =>
            {
                var zip = new SupportZip();
                zip.Open(validZipFile);
            });
        }

        [Test]
        public void OpenValidSupportZip()
        {
            var validSupportZip = Path.Combine(_testPath, @"..\Files\iCAS-Support-20181024093728.zip");
            Assert.DoesNotThrow(() =>
            {
                var zip = new SupportZip();
                zip.Open(validSupportZip);
            });
        }
    }
}
