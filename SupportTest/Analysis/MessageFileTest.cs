﻿using NUnit.Framework;
using Support.Analysis;
using System.IO;
using System.Text;

namespace SupportTest.Analysis
{
    [TestFixture]
    public class MessageFileTests
    {
        [Test]
        public void TestExampleIsParsedCorrectly()
        {
            using (var memStream = new MemoryStream(Encoding.UTF8.GetBytes(getExampleContent())))
            {
                var msgFile = new MessageFile(memStream);
                Assert.AreEqual("4.7.1", msgFile.DotNetVersion);
                Assert.AreEqual("3.7.2.0 QS5", msgFile.iCASVersion);
                Assert.AreEqual("iTernity GmbH", msgFile.LicenseeName);
                Assert.AreEqual("fake@mail.de", msgFile.LicenseeMail);
                Assert.AreEqual("79111 Freiburg", msgFile.LicenseeAddress);
                Assert.AreEqual("C50001112345", msgFile.LicenseNumber);
                Assert.AreEqual("Windows 2016", msgFile.OperatingSystem);
                Assert.AreEqual("Off", msgFile.Virtualisation);
                Assert.AreEqual(@"20 Core(s) at 2397 MHz(AES NI is supported)
127.9 GB of RAM
Ethernet 5x 100 MB, 0x 1 GB, 1x 10 GB
0.0 s for SystemInfo call
1.2 s for disk write test on drive C:\", msgFile.Details);
                Assert.IsTrue(msgFile.ClusterFeatureInstalled);
                Assert.IsTrue(msgFile.ServicesAreInCluster);
            }
        }

        private static string getExampleContent()
        {
            return @"iCASPowerShell Version: 3.7.2.0
Connection: localhost

Licensee:       iTernity GmbH
Licensee Mail:   fake@mail.de
Licensee Address: 79111 Freiburg
License Number: C50001112345


-- - Customer Message-- -
---Customer Message-- -
---Performance-- -
Processor:	Very Good
AES & HASH Performance: N/A
RAM: Excellent
WebService:	Excellent
Disk:		Good
Overall:		Good
Details:
20 Core(s) at 2397 MHz(AES NI is supported)
127.9 GB of RAM
Ethernet 5x 100 MB, 0x 1 GB, 1x 10 GB
0.0 s for SystemInfo call
1.2 s for disk write test on drive C:\

---Performance-- -
---Support Info-- -
OS:		Windows 2016
.Net Version:		4.7.1
iCAS Version:	3.7.2.0 QS5
Virtualisation:	Off
Cluster Feature installed: True
iCAS Services in Cluster: True
Cluster Name: iMMClu
Cluster Group Name: Role
Cluster Group State: Online
Active Node: iMM02

Cluster Node States:

Name                 ID    State
---------- -
iMM01                1     Down
iMM02                2     Up



Cluster Resources:

Name                State  OwnerGroup    ResourceType
-------- - ----------------------
Cluster IP Address  Online Cluster Group IP Address
Cluster Name        Failed Cluster Group Network Name
Ei Pi               Online Role          IP Address
iCAS Target Monitor Online Role          iCAS Target Monitor
iFSG                Online Role          Generic Service
iTernityService     Online Role          Generic Service


-- - Support Info-- -
ignoreFreeSpace.txt not found under: C:\Program Files\iTernity\iServer\ignoreFreeSpace.txt
AllowedPath.txt not found under: C:\Program Files\iTernity\iServer\AllowedPath.txt
AllowedMaxObjects.txt not found under: C:\Program Files\iTernity\iServer\AllowedMaxObjects.txt
AllowedServices.txt not found under: C:\Program Files\iTernity\iServer\AllowedServices.txt
AllowedShares.txt not found under: C:\Program Files\iTernity\iServer\AllowedShares.txt
AllowNotToCheckDomainUser.txt not found under: C:\Program Files\iTernity\iServer\AllowNotToCheckDomainUser.txt
AllowSeparatedWriteCache.txt not found under: C:\Program Files\iTernity\iServer\AllowSeparatedWriteCache.txt
AllowStartupWizard.txt not found under: C:\Program Files\iTernity\iServer\AllowStartupWizard.txt


-- Service Users--
iTernityService.exe.\icas_usr
iFSG.exe.\icas_usr";
        }
    }
}
