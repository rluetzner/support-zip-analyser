﻿using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Support.Analysis.Settings;

namespace SupportTest.Analysis
{
    [TestFixture]
    public class AppSettingsTest
    {
        [Test]
        public void CompareDefaults()
        {
            var defaultWebServiceAppSettings = @"<?xml version=""1.0"" encoding=""utf-8""?>
<appSettings>
 <add key=""InstallPath"" value=""C:\Program Files\iTernity"" />
 <add key=""DemoLicenseRepository"" value=""s3"" />
 <add key=""LoggingPath"" value=""C:\iTernity\logs"" />
 <add key=""ConfigPath"" value=""C:\Program Files\iTernity\iServer"" />
 <add key=""ObjectStorageConfigPath"" value=""C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"" />
</appSettings>";
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(defaultWebServiceAppSettings));
            var compare = new AppSettings(memStream, "appSettings.config");
            Assert.AreEqual(0, compare.DifferingSettings.Count);
        }

        [Test]
        public void CompareMissingSetting()
        {
            var defaultWebServiceAppSettings = @"<?xml version=""1.0"" encoding=""utf-8""?>
<appSettings> 
 <add key=""DemoLicenseRepository"" value=""s3"" />
 <add key=""LoggingPath"" value=""C:\iTernity\logs"" />
 <add key=""ConfigPath"" value=""C:\Program Files\iTernity\iServer"" />
 <add key=""ObjectStorageConfigPath"" value=""C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"" />
</appSettings>";
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(defaultWebServiceAppSettings));
            var compare = new AppSettings(memStream, "appSettings.config");
            Assert.AreEqual(1, compare.DifferingSettings.Count);
            var missing = compare.DifferingSettings.First();
            Assert.AreEqual(string.Empty, missing.CustomerValue);
            Assert.AreEqual("InstallPath", missing.Key);
        }

        [Test]
        public void CompareAddedSetting()
        {
            var defaultWebServiceAppSettings = @"<?xml version=""1.0"" encoding=""utf-8""?>
<appSettings> 
 <add key=""InstallPath"" value=""C:\Program Files\iTernity"" />
 <add key=""DemoLicenseRepository"" value=""s3"" />
 <add key=""LoggingPath"" value=""C:\iTernity\logs"" />
 <add key=""ConfigPath"" value=""C:\Program Files\iTernity\iServer"" />
 <add key=""ObjectStorageConfigPath"" value=""C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"" />
<add key=""AddedSetting"" value=""This has been added"" />
</appSettings>";
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(defaultWebServiceAppSettings));
            var compare = new AppSettings(memStream, "appSettings.config");
            Assert.AreEqual(1, compare.DifferingSettings.Count);
            var added = compare.DifferingSettings.First();
            Assert.AreEqual(string.Empty, added.OriginalValue);
            Assert.AreEqual("AddedSetting", added.Key);
        }

        [Test]
        public void CompareChangedSetting()
        {
            var defaultWebServiceAppSettings = @"<?xml version=""1.0"" encoding=""utf-8""?>
<appSettings> 
 <add key=""InstallPath"" value=""Different Install Path"" />
 <add key=""DemoLicenseRepository"" value=""s3"" />
 <add key=""LoggingPath"" value=""C:\iTernity\logs"" />
 <add key=""ConfigPath"" value=""C:\Program Files\iTernity\iServer"" />
 <add key=""ObjectStorageConfigPath"" value=""C:\Program Files\iTernity\iServer\iTernity.ObjectStorage.config"" />
</appSettings>";
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(defaultWebServiceAppSettings));
            var compare = new AppSettings(memStream, "appSettings.config");
            Assert.AreEqual(1, compare.DifferingSettings.Count);
            var changed = compare.DifferingSettings.First();
            Assert.AreEqual(@"C:\Program Files\iTernity", changed.OriginalValue);
            Assert.AreEqual("InstallPath", changed.Key);
            Assert.AreEqual("Different Install Path", changed.CustomerValue);
        }
    }
}
