﻿using System.IO;
using System.Linq;
using NUnit.Framework;
using Support.Analysis.Stats;

namespace SupportTest.Analysis
{
    [TestFixture]
    public class StatisticsTests
    {
        private static string _filePath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Files", "Statistic.xml");

        [Test]
        public void TestExampleIsParsedCorrectly()
        {
            using (var fs = new FileStream(_filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var statistics = new Statistics(fs);
                Assert.AreEqual(1, statistics.Years.Count);
                var year = statistics.Years.First();
                Assert.AreEqual(2019, year.Year);
                Assert.AreEqual(12, year.Months.Count);
                for (var i = 0; i < 12; i++)
                {
                    Assert.AreEqual(i + 1, year.Months[i].Month);
                }
                Assert.AreEqual(27895, statistics.FilesPerMonth);
                Assert.AreEqual(526234.9412797992, statistics.AverageFileSize, 0.01);
            }
        }
    }
}
