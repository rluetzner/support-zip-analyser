﻿using NUnit.Framework;
using Support.Analysis.EventLogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SupportTest.Analysis
{
    [TestFixture]
    public class EventLogHtmlTests
    {
        [Test]
        public void TestEmptyHtmlIsNotParsed()
        {
            var testHtml = "";
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(testHtml));
            var eventHtml = new EventLogHtml(memStream, "test.html");
            Assert.IsFalse(eventHtml.EventLogEntries.Any());
        }

        [Test]
        public void TestHtmlWithoutEventsIsEmpty()
        {
            var html = embedEventsInHtml(Enumerable.Empty<EventLogEntry>());
            using (var memStream = getMemoryStreamFromString(html))
            {
                var eventHtml = new EventLogHtml(memStream, "test.html");
                Assert.IsFalse(eventHtml.EventLogEntries.Any());
            }
        }

        private static string embedEventsInHtml(IEnumerable<EventLogEntry> events)
        {
            var (header, footer) = generateOuterHtml();
            return header + string.Join("", events.Select(e => generateFakeHtmlForEvent(e))) + footer;
        }

        private static MemoryStream getMemoryStreamFromString(string content)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(content));
        }

        [Test]
        public void TestIfSingleEventIsParsed()
        {
            var evt = new EventLogEntry
            {
                Category = 1000,
                EventId = 10,
                EntryType = "Error",
                Date = DateTime.Now,
                Message = "Test Event Log"
            };
            var html = embedEventsInHtml(new[] { evt });
            using (var memStream = getMemoryStreamFromString(html))
            {
                var evtHtml = new EventLogHtml(memStream, "test.html");
                Assert.AreEqual(1, evtHtml.EventLogEntries.Count);
                var firstEvent = evtHtml.EventLogEntries.First();
                assertEventPropertiesAreEqual(evt, firstEvent);
            }
        }

        private static void assertEventPropertiesAreEqual(EventLogEntry setupEntry, EventLogEntry readEntry)
        {
            Assert.AreEqual(setupEntry.Category, readEntry.Category);
            Assert.AreEqual(setupEntry.EventId, readEntry.EventId);
            const string dateFormat = "yyyyMMddHHmmss";
            Assert.AreEqual(setupEntry.Date.ToString(dateFormat), readEntry.Date.ToString(dateFormat));
            Assert.AreEqual(setupEntry.EntryType, readEntry.EntryType);
            Assert.AreEqual(setupEntry.Message, readEntry.Message);
        }

        private static (string header, string footer) generateOuterHtml()
        {
            return ("<html><head><title>EventLog: iTernity</title>" +
"<style type=\"text/css\">table { width:600px; }td { border:1px solid #000; vertical-align:top; overflow:hidden; }</style></head><body>" +
"<p>" +
"<table style=\"border:1px solid #A0A0A0; vertical-align:top;background-color:#BFDBFF\">" +
"<tr><td style=\"width:140px;\">Date:</td><td><strong>10/24/2018</strong></td></tr>" +
"</table></p>", "<p>" +
"<table style=\"border:4px solid #FFB375; vertical-align:top;\">" +
"<tr><td style=\"color:red\">There are 1256 entrie(s) in the event log that was not included in the export.</td></tr>" +
"</table></p>" +
"</body></html>");
        }

        private static string generateFakeHtmlForEvent(EventLogEntry entry)
        {
            return "<p><table style=\"border:1px solid #A0A0A0; vertical-align:top;\">" +
$"<tr><td style=\"width:140px;\">EntryType</td><td><strong>{entry.EntryType}</strong></td></tr>" +
$"<tr><td style=\"width:140px;\">Category</td><td>({entry.Category})</td></tr>" +
$"<tr><td style=\"width:140px;\">InstanceId</td><td>{entry.EventId}</td></tr>" +
$"<tr><td style=\"width:140px;\">TimeGenerated</td><td>{entry.Date}</td></tr>" +
"<tr><td style=\"width:140px;\">UserName</td><td></td></tr>" +
$"<tr><td style=\"width:140px;\">Message</td><td>{entry.Message}</td></tr>" +
"</table></p>";
        }

        [Test]
        public void TestListOfEntriesIsParsed()
        {
            var setupEvents = Enumerable.Range(0, 1000)
                .Select(i => new EventLogEntry
                {
                    Category = 1000 + i,
                    EventId = i,
                    Date = DateTime.Now,
                    EntryType = i % 2 == 0 ? "Warning" : "Error",
                    Message = $"Entry {i}"
                })
                .ToList();
            var html = embedEventsInHtml(setupEvents);
            using (var memStream = getMemoryStreamFromString(html))
            {
                var eventHtml = new EventLogHtml(memStream, "test.html");
                Assert.AreEqual(setupEvents.Count, eventHtml.EventLogEntries.Count);
                for (int i = 0; i < setupEvents.Count; i++)
                {
                    assertEventPropertiesAreEqual(setupEvents[i], eventHtml.EventLogEntries[i]);
                }
            }
        }
    }
}
