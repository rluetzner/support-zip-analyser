﻿using NUnit.Framework;
using Support.Analysis;
using System.IO;

namespace SupportTest.Analysis
{
    [TestFixture]
    public class ChangesTests
    {
        private static string _pathToSampleFile = Path.Combine(TestContext.CurrentContext.TestDirectory, "Files", "User.config.Changes-201810240937.htm");

        [Test]
        public void ParseChanges()
        {
            using (var fs = new FileStream(_pathToSampleFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var changes = new Changes(fs);
                Assert.AreEqual(230, changes.SavedChanges.Count);
            }
        }        
    }
}
