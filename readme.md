# Support Zip Analyzer

This is a GUI tool which parses and aggregates informations from an iCAS Support Zip.

## Getting Started

The project can be built using Visual Studio.

As of now, there is no build script.

To analyze an iCAS Support Zip, simply drag and drop the zip file into the GUI window.

## Running the tests

All tests are written using nUnit and can be run using the nUnit Test Runner.

## Changelog

The Changelog can be found [here](./changelog.md).