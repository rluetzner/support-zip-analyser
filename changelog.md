# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased

### Added

- Telemetry that tracks usage and Exceptions.
- Parse Statistics file.

### Changed

- Fix an error where the Tool could not be exited correctly.

## 0.8 - 2019-03-08

### Changed

- Show details window for Grouped Event.
- Close details window by pressing the Escape key.
- Close details window by clicking outside the window.
- Focus details window when opened.
- Open details window centered on screen.
- Remove smooth scrolling due to performance issues.

## 0.7.1 - 2019-03-05

### Changed

- Fixed an issue where not all Event Logs in a Support Zip were loaded.
- Replaced cold by hot Observables to improve performance.
- Enabled smooth scrolling in the Event Log data grid.

## 0.7 - 2019-01-22

### Added

- Added icon.

## 0.6 - 2018-12-13

### Added
- AppSettings are checked against given Default files. Only differing, missing or added values are shown.

### Changed
- Replaced WinForms with WPF.

## 0.5 - 2018-12-13

### Added
- Checkbox to group Event Log entries.

## 0.4 - 2018-11-02

### Changed

- Fixed an issue where the Application could not handle multiple config changes with the same timestamp. This could happen by merging changes from the iCAS user config and the Object Store config.

### Removed
- Loading screen.

## 0.3 - 2018-11-02

### Added

- Clicking on an entry in the Event Log data grid opens a separate window, where the Event Log entry is formatted as XML. The text is selected by default and can be copied instantly with CTRL+C.

## 0.2 - 2018-10-31

### Changed

- Replaced most of the eventing with Reactive Extension.