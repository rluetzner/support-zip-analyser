﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using Support.Analysis.EventLogs;

namespace SupportGuiWpf
{
    /// <summary>
    /// Interaction logic for SingleDataForm.xaml
    /// </summary>
    public partial class SingleDataForm : Window
    {
        private readonly EventLogEntry _entry;
        private string _serializedContent;

        public SingleDataForm(EventLogEntry entry)
        {
            InitializeComponent();
            _entry = entry;
        }

        private async void SingleDataForm_Load(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                var xmlSer = new XmlSerializer(typeof(EventLogEntry));
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                xmlSer.Serialize(sw, _entry);
                _serializedContent = sb.ToString();
            });
            EventContent.Text = _serializedContent;
            EventContent.SelectionStart = 0;
            EventContent.SelectionLength = _serializedContent.Length;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.HasFlag(Key.Escape))
            {
                Close();
            }
        }
    }
}
