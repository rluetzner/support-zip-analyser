﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Support;
using Support.Analysis;
using Support.Analysis.EventLogs;
using Support.Analysis.Settings;
using DataFormats = System.Windows.DataFormats;
using DragDropEffects = System.Windows.DragDropEffects;
using DragEventArgs = System.Windows.DragEventArgs;
using MessageBox = System.Windows.MessageBox;

namespace SupportGuiWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SupportZip _zip;

        public MainWindow()
        {
            InitializeComponent();
        }        
       
        private async Task openSupportZipFromPath(string path)
        {
            _zip = null;
            AllowDrop = false;
            resetAnalysis();
            _zip = new SupportZip();
            
            ProgressBar.Visibility = Visibility.Visible;
            ProgressBar.IsIndeterminate = true;

            await Task.Run(() => _zip.Open(path));
            
            setMessagesInfoIfReady(_zip.MessageFile);
            setChangesIfReady(_zip.ConfigChanges);
            addNewEventLogsToDictionaryAndComboBox(_zip.EventLogHtmls);
            setAppSettingDifferences(_zip.AppSettings);

            ProgressBar.Visibility = Visibility.Hidden;
            AllowDrop = true;
        }


        private void setMessagesInfoIfReady(MessageFile mf)
        {
            CbCluster.IsChecked = mf.ServicesAreInCluster;
            Messages.SelectedObject = mf;
        }

        private void setChangesIfReady(Changes changes)
        {
            TreeViewChanges.ItemsSource = buildChangesTree(changes);            
        }

        private void addNewEventLogsToDictionaryAndComboBox(IEnumerable<EventLogHtml> eventLogs)
        {
            foreach (var eventLog in eventLogs)
            {
                ComboEventLogs.Items.Add(eventLog.Name);
            }
            ComboEventLogs.SelectedIndex = 0;
        }


        private void setAppSettingDifferences(IEnumerable<AppSettings> appSett)
        {
            var joined = appSett
                .SelectMany(sett => sett.DifferingSettings)
                .ToList();
            DataAppSettings.ItemsSource = joined;
        }

        private static IEnumerable<TreeViewItem> buildChangesTree(Changes configChanges)
        {
            var treeNodes = new List<TreeViewItem>();
            foreach (var configChange in configChanges.SavedChanges.OrderByDescending(change => change.SaveTime))
            {
                var treeNode = new TreeViewItem { Header = $"Config saved on {configChange.SaveTime}" };
                var subNodes = configChange.ChangedValues
                    .Select(change => $"{change.Key} changed from {change.Change.Old} to {change.Change.New}")
                    .Select(s => new TreeViewItem { Header = s });
                foreach (var treeViewItem in subNodes)
                {
                    treeNode.Items.Add(treeViewItem);
                }

                treeNodes.Add(treeNode);
            }

            return treeNodes;
        }

        private void resetAnalysis()
        {
            ComboEventLogs.Items.Clear();
            ComboEventLogs.Text = "";
            CbLogFilter.Items.Clear();
            CbLogFilter.Text = "";
            Messages.SelectedObject = null;
            DataEvents.DataContext = null;
            CbCluster.IsChecked = false;
            LLogStatistics.Content = "";
            LShownEvents.Content = "";
            DataAppSettings.ItemsSource = null;
            DataEvents.ItemsSource = null;
            TreeViewChanges.ItemsSource = null;
        }

        private void win_dragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effects = DragDropEffects.Copy;
        }

        private async void MainWindow_OnDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length > 1)
            {
                MessageBox.Show("Please analyze only one file at a time.", "Too many files", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            var file = files.FirstOrDefault();
            if (file == null) return;
            await openSupportZipFromPath(file);
        }

        private void ComboEventLogs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            setDataFromTuple();
        }

        private void tb_Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            setDataFromTuple();
        }

        private void cb_GroupEvents_Checked(object sender, RoutedEventArgs e)
        {
            setDataFromTuple();
        }

        private void setDataFromTuple()
        {
            var selectedEventLog = ComboEventLogs.SelectedIndex;
            var filterText = tb_Filter.Text;
            var eventTypeFilter = CbLogFilter.Text;
            var groupEvents = cb_GroupEvents.IsChecked.GetValueOrDefault(false);

            if (_zip == null || _zip.EventLogHtmls.Count <= selectedEventLog) return;
            IEnumerable<EventLogEntry> events = _zip.EventLogHtmls[selectedEventLog].EventLogEntries;
            if (!string.IsNullOrEmpty(eventTypeFilter)) events = events.Where(evt => evt.EntryType == eventTypeFilter);
            if (!string.IsNullOrEmpty(filterText))
            {
                var parsedAsInt = int.TryParse(filterText, out var evtInt);
                events = events.Where(evt =>
                    evt.Message.Contains(filterText) || parsedAsInt && (evt.Category == evtInt) ||
                    evt.EventId == evtInt);
            }

            if (groupEvents)
            {
                var start = DateTime.UtcNow;
                var groupedEvents = events
                    .GroupBy(evt => new { evt.Category, evt.EventId, evt.EntryType })
                    .Select(group =>
                        new GroupEvent(group.Key.Category, group.Key.EventId, group.Key.EntryType, group.ToList()))
                    .ToList();
                LShownEvents.Content = $"{groupedEvents.Count()} Event(s) shown";
                DataEvents.ItemsSource = groupedEvents;
            }
            else
            {
                var eventsList = events.ToList();
                LShownEvents.Content = $"{eventsList.Count} Event(s) shown";
                DataEvents.ItemsSource = eventsList;
            }


            CbLogFilter.Items.Clear();
            CbLogFilter.Items.Add("");
            var eventsGroupedByLevel = _zip.EventLogHtmls[selectedEventLog].EventLogEntries
                .GroupBy(evt => evt.EntryType);
            foreach (var group in eventsGroupedByLevel)
            {
                CbLogFilter.Items.Add(group.Key);
            }

            LLogStatistics.Content = string.Join("; ", eventsGroupedByLevel.Select(g => $"{g.Key}: {g.Count()}"));
        }

        private void DataEvents_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = DataEvents.SelectedItem;
            if (item == null) return;
            switch (item)
            {
                case EventLogEntry entry:
                    showSingleDataFormFor(entry);
                    break;
                case GroupEvent groupEntry:
                    var sampleEntry = new EventLogEntry
                    {
                        EntryType = groupEntry.EntryType,
                        Category = groupEntry.Category,
                        Date = groupEntry.LastOccurrence,
                        EventId = groupEntry.EventId,
                        Message = groupEntry.LastMessage
                    };
                    showSingleDataFormFor(sampleEntry);
                    break;
            }
        }

        private void showSingleDataFormFor(EventLogEntry evt)
        {
            var singleDataForm = new SingleDataForm(evt)
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            singleDataForm.Show();
            singleDataForm.Focus();
        }
    }
}