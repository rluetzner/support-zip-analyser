﻿using System;
using System.Collections.Generic;
using System.Linq;
using Support.Analysis.EventLogs;

namespace SupportGuiWpf
{
    internal class GroupEvent
    {
        public GroupEvent(int category, int eventId, string entryType, List<EventLogEntry> events)
        {
            Category = category;
            EventId = eventId;
            EntryType = entryType;
            Count = events.Count;
            // events in the HTML file are in reverse order
            FirstOccurrence = events.Last().Date;
            LastOccurrence = events.First().Date;
            LastMessage = events.Last().Message;
        }
        
        public int Category { get; set; }
        public int EventId { get; set; }
        public string EntryType { get; set; }
        public int Count { get; set; }
        public DateTime FirstOccurrence { get; set; }
        public DateTime LastOccurrence { get; set; }
        public string LastMessage { get; set; }
    }
}